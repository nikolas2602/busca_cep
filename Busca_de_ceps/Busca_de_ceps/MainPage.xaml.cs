﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;


namespace Busca_de_ceps
{
   
    public class Endereco
    {
        public string Cep { get; set; }
        public string Logradouro { get; set; }
        public string Complemento { get; set; }
        public string Bairro { get; set; }
        public string Localidade { get; set; }
        public string Uf { get; set; }
        public string Unidade { get; set; }
        public string Ibge { get; set; }
        public string Gia { get; set; }

        public string EnderecoCompleto { get; set; }
            
    }



    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public Endereco Endereco { get; set; }

        public MainPage()
        {
            




            InitializeComponent();
            this.Endereco = new Endereco();
            this.Endereco.Logradouro = "";
            this.BindingContext = this;


        }

        public async void BuscarEndereco()
        {
            Console.WriteLine("Iniciando consumo da API");

            Uri url = new Uri("https://viacep.com.br/ws/" + Endereco.Cep + "/json/");

            HttpResponseMessage httpResponse = await Services.HttpService.GetRequest(url.AbsoluteUri);

            if (httpResponse.IsSuccessStatusCode)
            {
                string stringResponse = httpResponse.Content.ReadAsStringAsync().Result;
                Console.WriteLine("\n=======");
                Console.WriteLine(stringResponse);

                Endereco End = Services.SerializationService.DeserializeObject<Endereco>(stringResponse);

                if (End.Logradouro == "")
                {
                    this.Endereco.EnderecoCompleto =
                        End.Localidade + " - " +
                        End.Uf;
                }
                else
                {
                    this.Endereco.EnderecoCompleto =
                    End.Logradouro + ", "
                    + End.Bairro + ", "
                    + End.Localidade + " - "
                    + End.Uf;
                }
                OnPropertyChanged(nameof(this.Endereco));

            }




        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            BuscarEndereco();
        }
    }
}

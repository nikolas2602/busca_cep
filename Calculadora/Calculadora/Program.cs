﻿using System;

namespace Calculadora
{
    class Program
    {
        static void Main(string[] args)
        {
            // Declare variáveis ​​e, em seguida, inicialize como zero.
            int num1 = 0; int num2 = 0;

            // Exibe o título como o aplicativo de calculadora do console C #.
            Console.WriteLine("Calculadora\r");
            Console.WriteLine("------------------------\n");

            // Peça ao usuário para digitar o primeiro número.
            Console.WriteLine("Digite um número e pressione Enter");
            num1 = Convert.ToInt32(Console.ReadLine());

            // Peça do usuário para digitar o segundo número.
            Console.WriteLine("Digite outro número e pressione Enter ");
            num2 = Convert.ToInt32(Console.ReadLine());

            // Peça ao usuário para escolher uma opção.
            Console.WriteLine("Escolha uma opção da seguinte lista:");
            Console.WriteLine("\ta - Soma");
            Console.WriteLine("\ts - Subtração");
            Console.WriteLine("\tm - Multiplicação");
            Console.WriteLine("\td - Divisão");
            Console.Write("Sua opção? ");

            // Use uma instrução para fazer as contas.
            switch (Console.ReadLine())
            {
                case "a":
                    Console.WriteLine($"Seu resultado: {num1} + {num2} = " + (num1 + num2));
                    break;
                case "s":
                    Console.WriteLine($"Seu resultado: {num1} - {num2} = " + (num1 - num2));
                    break;
                case "m":
                    Console.WriteLine($"Seu resultado: {num1} * {num2} = " + (num1 * num2));
                    break;
                case "d":
                    Console.WriteLine($"Seu resultado: {num1} / {num2} = " + (num1 / num2));
                    break;
            }
            // Aguarde a resposta do usuário antes de fechar.
            Console.Write("Pressione qualquer tecla para fechar o aplicativo do console da Calculadora ...");
            Console.ReadKey();
        }
    }
}